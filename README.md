# Log collector

This project provides the background program `logcollectord` that
monitors the creation of new logs on a reactor's workstation and then

* Extract the relevant data from the CSV log file, namely the timestamp,
  `Incident_Power`, and `Read_MFC_CH4` fields.

* Verifies these values and their format.

* Creates and signs an Ethereum transaction recording these values.

Configuration is defined in [configs/config.yaml](configs/config.yaml) with vars:
- from-private-key 
- to-address-hex

A companion command-line tool will be provided to record QA reports to
the block chain.

*Warning*: logs will be written to /var/log/ld-logcollectord.log by
default, make sure to use log rotation if the program runs for a long
time.
