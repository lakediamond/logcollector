package main

import (
	"bufio"
	"context"
	"crypto/ecdsa"
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"math/big"
	"os"
	"path/filepath"
	"strconv"
	"time"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/go-kit/kit/log"
	"github.com/spf13/viper"
)

const (
	collectedExtension = ".ld"
)

var (
	gitCommit string
	buildDate string
)

func main() {

	fmt.Printf("LakeDiamond: log collector - version %s-%s\n", buildDate, gitCommit[:4])
	fmt.Println("Copyright (c) LakeDiamond SA, 2018")

	// read config file
	c := config()
	var (
		logsPath     = c.GetString("logs-path")
		logfilePath  = c.GetString("logfile-path")
		ethereumUrl  = c.GetString("ethereum-url")
		powerMin     = c.GetInt32("power-min")
		powerMax     = c.GetInt32("power-max")
		ch4Min       = c.GetInt32("ch4-min")
		ch4Max       = c.GetInt32("ch4-max")
		periodHours  = c.GetDuration("period-hours")
		privateKey   = c.GetString("from-private-key")
		toAddressHex = c.GetString("to-address-hex")
	)

	// init logger
	logFileName := fmt.Sprintf(logfilePath)
	logFile, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0660)
	if err != nil {
		fmt.Printf("[WARN] logs: unable to open file '%v' to write logs: %v\n", logFileName, err)
		logFile = os.Stdout
	}
	defer logFile.Close()
	logger := log.NewJSONLogger(logFile)
	{
		logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
	}
	defer logger.Log("msg", "goodbye")

	logger.Log("msg", "config", "power-min", powerMin, "power-max", powerMax, "ch4-min", ch4Min, "ch4-max", ch4Max, "period-hours", periodHours, "logs-path", logsPath, "logfile-path", logfilePath)

	// check that path exists, do not look for content at thus point
	if _, err := os.Stat(logsPath); os.IsNotExist(err) {
		logger.Log("msg", "logs-path does not exist", "logs-path", logsPath)
		return
	}

	// check private key
	privKey, err := crypto.HexToECDSA(privateKey)
	if err != nil {
		logger.Log("msg", "failed to process private key", "error", err)
		return
	}
	publicKey := privKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		logger.Log("msg", "failed to convert public key", "error", err)
		return
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	toAddress := common.HexToAddress(toAddressHex)

	// for each file f process, write f.ld including the serialized data, or error
	for {
		// are file matching the pattern
		files, err := filepath.Glob(logsPath +
			string(os.PathSeparator) + "MID[0-9]*" + string(os.PathSeparator) + "*TRN.CSV")
		if err != nil {
			logger.Log("msg", "glob failed", "error", err)
		}
		logger.Log("msg", "glob suceeded", "nbfiles", len(files))

		// get date
		year, month, day := time.Now().Date()

		// look for files whose last log entry was yesterday

		for _, fileName := range files {

			fileLogger := log.With(logger, "filename", fileName)

			// open csv file, get last line
			csvFile, err := os.Open(fileName)
			if err != nil {
				fileLogger.Log("msg", "error opening csv file", "error", err)
				continue
			}
			defer csvFile.Close()
			fileLogger.Log("msg", "reading csv file")
			// can be optimized if know the approximate length of a line
			scanner := bufio.NewScanner(csvFile)
			var lastLine string
			for scanner.Scan() {
				lastLine = scanner.Text()
			}
			fileLogger.Log("msg", "file read", "last line", lastLine)

			// compare date
			logDay, err := strconv.Atoi(lastLine[:2])
			if err != nil {
				fileLogger.Log("msg", "failed to convert day", "string", lastLine[:2])
				continue
			}
			logMonth, err := strconv.Atoi(lastLine[3:5])
			if err != nil {
				fileLogger.Log("msg", "failed to convert month", "string", lastLine[3:5])
				continue
			}
			logYear, err := strconv.Atoi(lastLine[6:10])
			if err != nil {
				fileLogger.Log("msg", "failed to convert year", "string", lastLine[6:10])
				continue
			}
			fileLogger.Log("msg", "last line date", "day", logDay, "month", logMonth, "year", logYear)
			nowDate := time.Date(year, month, day, 0, 0, 0, 0, time.UTC)
			logDate := time.Date(logYear, time.Month(logMonth), logDay, 0, 0, 0, 0, time.UTC)
			days := int(nowDate.Sub(logDate) / (24 * time.Hour))
			fileLogger.Log("msg", "difference with now", "days", days)

			// only process files whose last log was yesterday
			if days != 1 {
				continue
			}

			// get hash of the file
			//reader := csv.NewReader(bufio.NewReader(csvFile))
			toHash, err := ioutil.ReadAll(csvFile)
			if err != nil {
				fileLogger.Log("msg", "failed to read csv file", "error", err)
			}
			hashValue := crypto.Keccak256(toHash)

			// create aggregate (then hash it):
			//   for each line, if status change then write it to fileName.ld
			csvReader := csv.NewReader(bufio.NewReader(csvFile))
			// field delimiter is ';'
			csvReader.Comma = ';'
			// true when Incident_Power (field 2) >= 1000 and Read_MFC_CH4 (field 6) != 0
			running := false
			fileLogger.Log("msg", "reading csv file")
			// go back to file start
			csvFile.Seek(0, 0)
			// create log summary file
			sumFile, err := os.Create(fileName + ".ld")
			if err != nil {
				fileLogger.Log("msg", "can't create summary file", "error", err)
				continue
			}
			defer sumFile.Close()
			for {
				line, err := csvReader.Read()
				if err == io.EOF {
					break
				} else if err != nil && line == nil {
					fileLogger.Log("msg", "error reading csv file", "error", err)
					continue
				}
				// ignore the line if it doesn't include all fields
				if len(line) != 16 {
					continue
				}
				incidentPower, err := strconv.Atoi(line[2])
				if err != nil {
					fileLogger.Log("msg", "failed to convert incident power", "string", line[2])
					continue
				}
				CH4, err := strconv.Atoi(line[6])
				if err != nil {
					fileLogger.Log("msg", "failed to convert CH4", "string", line[6])
					continue
				}
				if incidentPower >= 1000 && CH4 != 0 {
					// machine producing diamond
					if !running { // wasnt running, alert that starting now
						running = true
						fmt.Fprintf(sumFile, "%s:%s: machine started (%d, %d)\n", line[0], line[1], incidentPower, CH4)
					}
				} else {
					// machine not producing diamond
					if running { // was running, alert that now stopped
						running = false
						fmt.Fprintf(sumFile, "%s:%s: machine stopped (%d, %d)\n", line[0], line[1], incidentPower, CH4)
					}
				}
			}
			// reset point and read all the file
			sumFile.Seek(0, 0)
			summary, err := ioutil.ReadAll(sumFile)
			if err != nil {
				fileLogger.Log("msg", "failed to read summary file", "error", err)
				continue
			}
			hashValueSummary := crypto.Keccak256(summary)

			dataToLog := append(hashValue, hashValueSummary...)

			// create Eth transaction
			client, err := ethclient.Dial(ethereumUrl)
			if err != nil {
				fileLogger.Log("msg", "ethclient failed to connect", "error", err)
				continue
			}
			nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
			if err != nil {
				fileLogger.Log("msg", "failed to get nonce", "error", err)
				continue
			}
			fileLogger.Log("msg", "fetched nonce value", "nonce", nonce)

			balance, err := client.BalanceAt(context.Background(), fromAddress, nil)
			if err != nil {
				fileLogger.Log("msg", "failed to get balance", "error", err)
				continue
			}
			fileLogger.Log("msg", "account balance", "balance", balance)

			value := big.NewInt(1) // in wei

			gasLimit, err := client.EstimateGas(context.Background(), ethereum.CallMsg{
				To:   &toAddress,
				Data: dataToLog,
			})
			if err != nil {
				fileLogger.Log("msg", "failed to get gas limit", "error", err)
				continue
			}
			fileLogger.Log("msg", "suggested gas limit", "gaslimit", gasLimit)

			gasPrice, err := client.SuggestGasPrice(context.Background())
			if err != nil {
				fileLogger.Log("msg", "failed to get gas price", "error", err)
				continue
			}
			fileLogger.Log("msg", "suggested gas price", "gasprice", gasPrice)

			tx := types.NewTransaction(nonce, toAddress, value, gasLimit, gasPrice, dataToLog)
			signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, privKey)
			if err != nil {
				fileLogger.Log("msg", "failed to create signed transaction", "error", err)
				continue
			}
			fileLogger.Log("msg", "transaction created")
			err = client.SendTransaction(context.Background(), signedTx)
			if err != nil {
				fileLogger.Log("msg", "failed to send transaction", "error", err)
				continue
			}
			fileLogger.Log("msg", "transaction sent", "txhash", signedTx.Hash().Hex())
		}

		// wait
		logger.Log("msg", "waiting..", "hours", periodHours)
		time.Sleep(periodHours)
	}
}

func config() *viper.Viper {

	var v = viper.New()
	v.SetConfigName("config")
	v.AddConfigPath(".")
	v.AddConfigPath("./configs")
	v.AddConfigPath("../configs")
	v.SetDefault("logs-path", "./example/")
	v.SetDefault("logfile-path", "/var/log/logcollector.d")
	v.SetDefault("ethereum-url", "https://rinkeby.infura.io")
	v.SetDefault("power-min", 0)
	v.SetDefault("power-max", 6000)
	v.SetDefault("ch4-min", 0)
	v.SetDefault("ch4-max", 200)
	v.SetDefault("period-hours", "24h")
	v.SetDefault("from-private-key", "")
	v.SetDefault("to-address-hex", "")

	err := v.ReadInConfig()
	if err != nil {
		fmt.Printf("[ERROR] error reading config: %v\n", err)
		os.Exit(1)
	}

	return v
}
