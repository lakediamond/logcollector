#!/bin/bash

echo "LakeDiamong log collector build script"
echo "Copyright (c) LakeDiamond SA, 2018"
echo ""

if ! [ -x "$(command -v goimports)" ]; then 
    echo "Error: goimports not found (or not on the path)"
    echo "To get run go get golang.org/x/tools/cmd/goimports and add \$GOPATH/bin to your path."
    exit 1
fi

CMDPATH=lakediamond/logcollector/cmd/logcollectord

goimports -w $GOPATH/src/$CMDPATH

GIT_COMMIT=$(git rev-list -1 HEAD)
NOW=$(date "+%Y%m%d")

# see valid values at https://gist.github.com/asukakenji/f15ba7e588ac42795f421b48b8aede63
#GOOS=linux 

printf "building logcollectord:\n\tversion $NOW-$GIT_COMMIT\n\tOS $GOOS\n\tarch: $GOARCH\n"

GOOS=darwin
GOARCH=amd64
printf "=> building logcollectord for mac...\n"
GOOS=$GOOS GOARCH=$GOARCH go build -o bin/logcollectord -ldflags "-X main.gitCommit=$GIT_COMMIT -X main.buildDate=$NOW" $CMDPATH

GOOS=windows
GOARCH=amd64
#GOARM=7
printf "=> building logcollectord for windows...\n"
CC=x86_64-w64-mingw32-gcc CGO_ENABLED=1 GOOS=$GOOS GOARCH=$GOARCH go build -o bin/logcollectord.exe -ldflags "-X main.gitCommit=$GIT_COMMIT -X main.buildDate=$NOW" $CMDPATH
